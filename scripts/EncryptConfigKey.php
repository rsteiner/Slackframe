<?php
require_once 'vendor/autoload.php';
//php vendor/CharityRootInc/config/src/encrypt.php THE_KEY THE_VALUE
echo PHP_EOL . 'Copy and paste the following into your config file: ' . PHP_EOL;
echo $argv[1] . ' = "' . Rsteiner\Slackframe\Config::buildEncryptedKey($argv[2]) . '"' . PHP_EOL . PHP_EOL;
