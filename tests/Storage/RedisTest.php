<?php
use Rsteiner\Slackframe\Framework;

class RedisTest extends PHPUnit_Framework_TestCase {

    public function testRedisSetGetDelete()
    {
        $instance = new Application;

        // Test string
        $this->assertTrue($instance->getStorage()->set('test_string', 'Hello'));
        $this->assertTrue($instance->getStorage()->get('test_string') === 'Hello');
        $this->assertTrue($instance->getStorage()->delete('test_string'));
        $this->assertFalse($instance->getStorage()->get('test_string'));

        // Test boolean serialization
        $this->assertTrue($instance->getStorage()->set('test_bool', true));
        $this->assertTrue($instance->getStorage()->get('test_bool'));
        $this->assertTrue($instance->getStorage()->delete('test_bool'));
        $this->assertFalse($instance->getStorage()->get('test_bool'));

        // Test array serialization
        $this->assertTrue($instance->getStorage()->set('test_array', ['test' => 123]));
        $this->assertTrue(is_array($instance->getStorage()->get('test_array')));
        $this->assertTrue($instance->getStorage()->get('test_array')['test'] === 123);
        $this->assertTrue($instance->getStorage()->delete('test_array'));
        $this->assertFalse($instance->getStorage()->get('test_array'));

        // Test class serialization
        $class = new Exception('TEST');
        $this->assertTrue($instance->getStorage()->set('test_class', $class));
        $class = $instance->getStorage()->get('test_class');
        $this->assertTrue($class->getMessage() === 'TEST');
        $this->assertTrue($instance->getStorage()->delete('test_class'));
        $this->assertFalse($instance->getStorage()->get('test_class'));

    }

}
