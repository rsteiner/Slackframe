<?php
use Rsteiner\Slackframe\Framework\Rtm;

class RtmTest extends PHPUnit_Framework_TestCase {

    /**
     * Test RTM Connection
     *
     * This test just connects and disconnects. We're really just
     * checking for Exceptions.
     *
     * @todo Install VCR or similar request recording and simulating utility.
     *
     * @return void
     */
    public function testConnect()
    {
        $instance = Rtm::instance();
        $instance->connect();
    }

}
