<?php
use Rsteiner\Slackframe\Framework;

class FrameworkTest extends PHPUnit_Framework_TestCase {

    public function testInstance()
    {
        $app = Framework::instance();
        $this->assertTrue(get_class($app) === 'Rsteiner\Slackframe\Framework');
    }

    /**
     * Use Reflection to Access Protected Methods
     * @param  string $name method name
     * @return ReflectionMethod
     */
    protected static function _getMethod(string $name): ReflectionMethod
    {
        $class = new ReflectionClass('Rsteiner\Slackframe\Framework');
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }

    protected static function _getConstant(string $name)
    {
        $class = new ReflectionClass('Rsteiner\Slackframe\Framework');
        return $class->getConstant($name);
    }

}
