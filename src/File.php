<?php
namespace Rsteiner\Slackframe;
use GuzzleHttp\Client;

class File extends Client {
    const REQUEST_OK    = 200;
    const METHOD_GET    = 'get';
    const METHOD_POST   = 'post';
    const METHOD_PUT    = 'put';
    const METHOD_DELETE = 'delete';
    protected $_response;
    protected $_body;
    protected $_query = [];
    protected $_json = [];

    function __construct(array $config = [])
    {
        $this->_bearer = Config::instance()->get('Slack', 'TEAM_TOKEN');

        parent::__construct($config);
    }

    public static function instance(array $config = [])
    {
        return new static($config);
    }

    public function download(string $endpoint, string $target)
    {
        $data = $this->request(self::METHOD_GET, $endpoint, [
            'exceptions' => FALSE,
            'headers' => [
                'Authorization' => 'Bearer ' . $this->_bearer,
            ]
        ])
        ->getBody()
        ->getContents();

        return file_put_contents($target, $data);
    }
}
