<?php
namespace Rsteiner\Slackframe;

/**
 * Core Game Class
 *
 * @category   Core
 * @package   Rsteiner\Slackframe
 */
class Framework {

    /**
     * Redis storage engine name
     *
     * @var string
     */
    protected const _STORAGE_REDIS = 'redis';

    /**
     * MySql storage engine name (inactive)
     *
     * This is not currently in use. This application is built to allow for
     * easy installation of new storage engines.
     *
     * @var string
     */
    protected const _STORAGE_MYSQL = 'mysql';

    /**
     * Mongo storage engine name (inactive)
     *
     * @var string
     */
    protected const _STORAGE_MONGO = 'mongo';

    /**
     * Default static configuration
     * @var array
     */
    protected const _CONFIG = [
        'storage' => [
            'default' => self::_STORAGE_REDIS
        ]
    ];

    /**
     * Stored storage engine
     * @var Storage
     */
    protected $_storage;

    /**
     * Contructor
     *
     * Instantiate storage engine.
     */
    function __construct()
    {
        $this->_initCache();
    }

    /**
     * Instantiate Framework
     *
     * @return self New instance
     */
    public static function instance(): self
    {
        return new static;
    }

    /**
     * Retrieve Storage Engine
     *
     * @return Storage Storage engine extension utility
     */
    public function getStorage(): Storage
    {
        return $this->_storage;
    }

    /**
     * Initiate the Storage Engine
     *
     * @return void
     */
    protected function _initCache(): void
    {
        $class_name = '_init' . ucwords(static::_CONFIG['storage']['default']);
        if (!method_exists($this, $class_name)) {
            throw new Exception('The ' . static::_CONFIG['storage']['default']
                . ' storage class has not been configured.');
        }

        $this->$class_name();
    }

    /**
     * Initiate the Redis Storage Engine
     *
     * @return void
     */
    protected function _initRedis(): void
    {
        $this->_storage = new Storage\Redis;
    }

}
