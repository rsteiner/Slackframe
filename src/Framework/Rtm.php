<?php
namespace Rsteiner\Slackframe\Framework;

/**
 * RTM Bot Class
 *
 * @category   Core
 * @category   RTM
 * @package   Rsteiner\Slackframe
 */
abstract class Rtm extends Framework implements RtmInterface{
    use RtmTrait;
}
