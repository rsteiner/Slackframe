<?php
namespace Rsteiner\Slackframe\Framework;
use Rsteiner\Slackframe\Framework, ArrayObject;

/**
 * Request Handler Class
 *
 * @category   Controller
 * @package    Rsteiner\Slackframe
 */
class Request extends Framework {
    protected $_headers = [];
    protected $_post = [];

    function __construct()
    {
        parent::__construct();
        $this->_headers = new ArrayObject(getallheaders());
        $this->_post = new ArrayObject($_POST ?: []);
        $this->_init();
    }

    /**
     * Check for Valid Verification Token
     *
     * @return boolean
     */
    public function isValid(): bool
    {
        return $this->_post->offsetGet('token') === $this->_token;
    }

    /**
     * Retrieve Request Headers
     *
     * @return ArrayObject
     */
    public function getHeaders(): ArrayObject
    {
        return $this->_headers;
    }

    /**
     * Get Post Data
     *
     * @return ArrayObject [description]
     */
    public function getPost(): ArrayObject
    {
        return $this->_post;
    }

    /**
     * Render JSON Response
     */
    public function render()
    {
        $this->_processCommand();
        http_response_code(200);
        header('Content-Type: application/json');
        echo json_encode([
            'response_type' => 'in_channel',
            'text' => $this->_response_text
        ]);
        exit;
    }

    /**
     * Initiate the Request
     * @return void
     */
    protected function _init(): void
    {
        $this->_game['id'] = $this->_post->offsetGet('channel_id') . '_dm';
        $this->_game = $this->_getGame();
        if (!$this->_game['user']) {
            $this->_game['user'] = $this->_post->offsetGet('user_id');
            $this->_game['user.name'] = $this->_post->offsetGet('user_name');
        }
        else if (!$this->_game['user2'] && $this->_post->offsetGet('user_id') !== $this->_game['user']) {
            $this->_game['user2'] = $this->_post->offsetGet('user_id');
            $this->_game['user2.name'] = $this->_post->offsetGet('user_name');
        }

        $this->_setContext($this->_post->offsetGet('text'));
    }

}
