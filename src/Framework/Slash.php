<?php
namespace Rsteiner\Slackframe\Framework;
use Rsteiner\Slackframe\Framework\Request;

/**
 * Slash command handler Class
 *
 * @category   Core
 * @category   Slash
 * @package   Rsteiner\Slackframe
 */
class Slash extends Request {

    /**
     * The application slash command
     *
     * This is used for help information only.
     *
     * @var string
     */
    protected const _SLASH = 'ttt';

    /**
     * Stored response
     * @var string
     */
    protected $_response_text = '';

    /**
     * Verification token
     * @var string
     */
    protected $_token;

    /**
     * ID of the bot player (Inactive)
     * @var string
     */
    protected $_bot_player;

    function __construct()
    {
        $this->_token = Config::get('Slack', 'VERIFICATION_TOKEN');
        if (!$this->_token) {
            throw new Exception('"Slack:VERIFICATION_TOKEN" config variable must be set');
        }

        parent::__construct();
    }

    /**
     * Process Slash Command
     *
     * Process a slash command after the context has been set.
     *
     * @return void
     */
    protected function _processCommand(): void
    {
        switch ($this->_context) {
            case self::_CONTEXT_NEWGAME:
                $this->getStorage()->delete($this->_game['id']);
                $this->_resetGame();
                if ($this->_post->offsetGet('channel_name') === 'directmessage') {
                    $this->_response_text .= PHP_EOL . $this->_compileBoard();
                }
                else {
                    $this->_response_text .= PHP_EOL
                        . 'To create a new game in a channel, you must challenge a '
                        . 'team member. ```/' . self::_SLASH . ' @example-team-member```';
                }

                break;

            case self::_CONTEXT_MARK:
                $this->_response_text = $this->_compileMarkResponse();
                break;

            case self::_CONTEXT_SET_ICON:
                $user_icon = $this->_post->offsetGet('text');
                $user_icon = substr($user_icon, 0, strrpos($user_icon, ':')) . ':';
                $this->_setIcon($this->_post->offsetGet('user_id'), $user_icon);
                $this->_response_text = 'Your icon has been updated. ' . $user_icon;
                break;

            case self::_CONTEXT_HELP:
                $this->_response_text = $this->_getHelpCopy();
                break;

            case self::_CONTEXT_CHALLENGE:
                if (ltrim($this->_post->offsetGet('text'), '@') === $this->_post->offsetGet('user_name')) {
                    $this->_response_text .= PHP_EOL . 'You can\'t challenge yourself, weirdo.';
                    break;
                }

                $this->getStorage()->delete($this->_game['id']);
                $this->_resetGame();
                $this->_game['user2.name'] = ltrim($this->_post->offsetGet('text'), '@');
                $this->_game['user2'] = null;
                $this->_saveGame();
                $this->_response_text .= PHP_EOL . $this->_compileBoard();
                break;

            case self::_CONTEXT_STATUS:
                $this->_response_text = $this->_compileBoard();
                break;

            default:
                $this->_response_text = 'Invalid command.' . PHP_EOL
                    . '*Help Command*: ```/' . self::_SLASH . ' help```';
                break;

        }

    }

    /**
     * Compile the game board
     *
     * @return string
     */
    protected function _compileBoard(): string
    {
        $text = intval($this->_post->offsetGet('text')) && !$this->_winner
            ? ($this->_post->offsetGet('user_name') . ': '
                . intval($this->_post->offsetGet('text')) . PHP_EOL)
            : '';

        if ($this->_game['user']) {
            $text .=  $this->_game['user.name'] . ': '
                . $this->_getUserIcon($this->_game['user']) . PHP_EOL;
        }

        if ($this->_game['user2.name'] && !$this->_game['user2']) {
            $text .=  $this->_game['user2.name'] . ': '
                . self::_CONFIG['user2_icon'] . PHP_EOL;
        }
        else if ($this->_game['user2']) {
            $text .=  $this->_game['user2.name'] . ': '
                . $this->_getUserIcon($this->_game['user2']) . PHP_EOL;
        }

        $text .= PHP_EOL . parent::_compileBoard();
        if (!$this->_winner) {
            $text .= PHP_EOL . 'Your turn, <@' . $this->_getNextTurnUser() . '>';
        }

        return $text;
    }

    /**
     * Compile full mark response
     *
     * @return string
     */
    protected function _compileMarkResponse(): string
    {
        if ($this->_post->offsetGet('user_id') === $this->_game['last']) {
            return 'It\'s not your turn, ' . $this->_post->offsetGet('user_name');
        }

        /*
         * @todo Allow users to play against the bot in channels
        if ($this->_getNextTurnUser() === $this->_bot_player) {
            $this->_mark($this->_bot_player, $this->_getBotMove());
            $this->_compileMarkResponse();
        }
        */

        if (!$this->_validateMark($this->_post->offsetGet('text'))) {
            return $this->_validation_message;
        }

        $this->_mark($this->_post->offsetGet('user_id'), intval($this->_post->offsetGet('text')));
        if ($this->_isWinner()) {
            $text  = $this->_compileBoard() . PHP_EOL . PHP_EOL
                . $this->_post->offsetGet('user_name') . ' wins.' . PHP_EOL
                . self::_GIPHY_BASE
                . self::_LOSER_GIFS[array_rand(self::_LOSER_GIFS)]
                . '/giphy.gif?ts=' . time() . rand(100, 999)
                . PHP_EOL . 'Run it back...' . PHP_EOL;

            $this->_resetGame();
            $text .= $this->_compileBoard();

            return $text;
        }

        $this->_saveGame();

        return $this->_compileBoard();
    }

    /**
     * Find out who's next
     *
     * @return string The Id or name of the next user
     */
    protected function _getNextTurnUser(): string
    {
        return $this->_game['last'] === $this->_game['user']
            ? ($this->_game['user2'] ?: $this->_game['user2.name'])
            : $this->_game['user'];
    }

}
