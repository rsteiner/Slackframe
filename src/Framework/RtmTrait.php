<?php
namespace Rsteiner\Slackframe\Framework;
use Rsteiner\Slackframe\Config;
use Exception,
    React\EventLoop\Factory as Looper,
    Slack\RealTimeClient;

trait RtmTrait {

    /**
     * RTM Helpers
     * @var array
     */
    private $_realtime = [
        'loop' => null,
        'client' => null,
        'data' => null,
        'channel' => null
    ];

    /**
     * Bot token
     * @var string
     */
    private $_token;

    /**
     * Contructor
     *
     * Instantiate environmental variables.
     */
    function __construct()
    {
        $this->_token = Config::instance()->get('Slack', 'RTM_TOKEN');
        if (!$this->_token) {
            throw new Exception('"Slack:RTM_TOKEN" Config variable must be set');
        }

        $this->_bot_id = Config::instance()->get('Slack', 'BOT_ID');
        if (!$this->_token) {
            throw new Exception('"Slack:BOT_ID" config variable must be set');
        }

        parent::__construct();
    }

    /**
     * Connect RTM Websocket
     *
     * @return void
     */
    public function connect(): void
    {
        $this->_initListener();
    }

    /**
     * Disconnect RTM Websocket
     *
     * @return bool Disconnect successful
     */
    public function disconnect(): bool
    {
        if ($this->_realtime['client'] !== null) {
            return get_class($this->_realtime['client']->disconnect()) === 'React\Promise\RejectedPromise';
        }

        return false;
    }

    /**
     * Initate RTM Websocket Listener
     *
     * @return void
     */
    private function _initListener(): void
    {
        $this->_realtime['loop'] = Looper::create();
        $this->_realtime['client'] = new RealTimeClient($this->_realtime['loop']);
        $this->_realtime['client']->setToken($this->_token);

        $this->_realtime['client']->on('message', function ($data) {

            if ($this->_isInvalidPayload($data)) {
                return;
            }

            $this->_realtime['data'] = $data;
            $this->_realtime['channel'] = new \Slack\Channel(
                $this->_realtime['client'],
                ['id' => $this->_realtime['data']->offsetGet('channel')]);

            $this->_processCommand();

            return;
        });

        $this->_realtime['client']
            ->connect()
            ->then(function () {
                echo 'Connected to Slack...' . PHP_EOL . PHP_EOL;
                if (defined('IS_TESTING')) {
                    $this->disconnect();
                }

        });

        $this->_realtime['loop']->run();

    }

    /**
     * Post an RTM message
     *
     * @param  string $text The message text
     * @return void
     */
    protected function _sendMessage(string $text): void
    {
        $this->_realtime['client']
            ->postMessage($this->_realtime['client']
                ->getMessageBuilder()
                ->setText($text)
                ->setChannel($this->_realtime['channel'])
                ->create());
    }

    /**
     * Validate Message Payload
     *
     * @param  SlackPayload $data Message Payload
     * @return boolean
     */
    abstract protected function _isInvalidPayload(\Slack\Payload $data): bool;


    /**
     * Process RTM Command
     *
     * Process a RTM command after the context has been set.
     *
     * @return void
     */
    abstract protected function _processCommand(): void;

}
