<?php
namespace Rsteiner\Slackframe;
use ArrayObject;

/**
 * Secure Configuration Storage and Retrieval Utility
 *
 * Sample:
 * ; Databse configuration
 * [database]
 * name = "my_database_name"
 * user = "db_user"
 * password = "~encrypted[78d9das9879as7d9as7d9a]"
 * host = "localhost"
 */
class Config {
    const DEFAULT_DIR = 'Application/Config';
    const DEFAULT_FILE = 'main';
    private $_config;
    private $_group;
    private $_dir;
    private $_file;

    function __construct($file_name)
    {
        $this->_dir = rtrim(defined('CONFIG_DIR') ? CONFIG_DIR : self::DEFAULT_DIR, '/');
        $this->_file = defined('CONFIG_FILE') ? CONFIG_FILE : self::DEFAULT_FILE;

        $file_path = $this->_dir . '/' . $this->_file. '.ini';
        if (!file_exists($file_path)) {
            throw new \Exception('Config file not found. [' . $file_path . ']');
        }

        $this->_config = new ArrayObject(parse_ini_file($file_path, true));
    }

    public static function instance(string $file_name = null): self
    {
        return new self($file_name);
    }

    public function get(string $group, string $key)
    {
        $group = $this->_config->offsetGet($group);
        if ($group === null) {
            return;
        }

        $group = new ArrayObject($group);

        $value = $group->offsetGet($key);
        if ($value === null) {
            return;
        }

        $regex = "/~json\[(.*?)\]/";
        preg_match_all($regex, $value, $matches);

        if (isset($matches[1][0])) {
            return json_decode($matches[1][0] .']', true);
        }

        $regex = "/~encrypted\[(.*?)\]/";
        preg_match_all($regex, $value, $matches);
        if (!isset($matches[1][0])) {
            return $value;
        }

        return self::_decode($matches[1][0]) ?: null;
    }

    public static function buildEncryptedKey($value)
    {
        return '~encrypted[' . self::_encode($value) . ']';
    }

    private static function _encode($string) {

        $j = 0;
        $key = sha1(self::_getSalt());
        $strLen = strlen($string);
        $keyLen = strlen($key);
        $hash = '';

        for ($i = 0; $i < $strLen; $i++) {
            $ordStr = ord(substr($string, $i, 1));
            if ($j == $keyLen) {
                $j = 0;
            }

            $ordKey = ord(substr($key, $j, 1));
            $j++;
            $hash .= strrev(base_convert(dechex($ordStr + $ordKey), 16, 36));
        }
        return $hash;
    }

    private static function _decode($string) {
        $j = 0;
        $hash = '';
        $key = sha1(self::_getSalt());
        $strLen = strlen($string);
        $keyLen = strlen($key);
        for ($i = 0; $i < $strLen; $i += 2) {
            $ordStr = hexdec(base_convert(strrev(substr($string, $i, 2)), 36, 16));
            if ($j == $keyLen) { $j = 0;
            }

            $ordKey = ord(substr($key, $j, 1));
            $j++;
            $hash .= chr($ordStr - $ordKey);
        }
        return $hash;
    }

    private static function _getSalt()
    {
        return sha1(__CLASS__ . __FUNCTION__);
    }

}
