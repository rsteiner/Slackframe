<?php
namespace Rsteiner\Slackframe;

/**
 * Core Game Class
 *
 * @category   Core
 * @package   Rsteiner\Slackframe
 */
class Rest {

    protected const _ENDPOINT = 'https://slack.com/api/';

    public function get(string $action, array $params = [])
    {
        $params['token'] = self::_getApiToken();

        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', self::_ENDPOINT, $params);
        $res->getStatusCode();
        $res->getHeader('content-type');
        $body = $res->getBody();
    }

    protected function _getApiToken(): ?string
    {

    }

}
