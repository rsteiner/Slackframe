<?php
namespace Rsteiner\Slackframe;

/**
 * Base Storage Class
 *
 * @category   Storage
 * @package   Rsteiner\Slackframe
 */
abstract class Storage {
    abstract function get($key);
    abstract function set($key, $value, int $ttl = null): bool;
    abstract function delete($key): bool;
    abstract function deleteLike($key): void;
    abstract function flush(): bool;

    /**
     * Check if a String is Serialized
     * @param  string  $value
     * @return boolean        Is serialized
     */
    protected static function _is_serialized(string $value)
    {
        return @unserialize($value) !== false;
    }

}
