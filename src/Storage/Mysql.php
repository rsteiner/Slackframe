<?php
namespace Rsteiner\Slackframe\Storage;
use Rsteiner\Slackframe\Storage;

/**
 * MySql Storage Extension
 *
 * @category   Storage
 * @package   Rsteiner\Slackframe
 */
class Mysql extends Storage {
    private $_client;

    public function get($key)
    {}

    public function set($key, $value, int $ttl = null): bool
    {}

    public function delete($key): bool
    {}

    public function deleteLike($key): void
    {}

    public function flush(): bool
    {}

}
