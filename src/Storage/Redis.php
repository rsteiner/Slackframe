<?php
namespace Rsteiner\Slackframe\Storage;
use Rsteiner\Slackframe\Storage;

/**
 * Redis Storage Extension
 *
 * @category   Storage
 * @package   Rsteiner\Slackframe
 */
class Redis extends Storage {
    private $_client;

    /**
     * Redis Configuration
     * @var array
     */
    private const _CONFIG = [
        'host' => '127.0.0.1',
        'port' => 6379
    ];

    public function __construct()
    {
        $this->_client = new \Redis;
        $this->_client->pconnect(
            self::_CONFIG['host'],
            self::_CONFIG['port']);

        $this->_client->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_NONE);
    }

    /**
     * Retrieve Item by Key
     *
     * @param  $key
     */
    public final function get($key)
    {
        $return = $this->_client->get($key);

        if ($return && self::_is_serialized($return)) {
            $return = unserialize($return);
        }

        return $return;
    }

    /**
     * Set Item
     *
     * @param  $key
     * @param  $value
     * @param  int $ttl
     * @return bool
     */
    public final function set($key, $value, int $ttl = null): bool
    {
        if (!is_string($value) && !is_numeric($value)) {
            $value = serialize($value);
        }

        return $this->_client->set($key, $value);
    }

    /**
     * Delete item by Key
     * @param  key
     * @return bool
     */
    public function delete($key): bool
    {
        return $this->_client->delete($key);
    }

    /**
     * Delete item by Wildcard Key
     * @param $key
     */
    public function deleteLike($key): void
    {
        foreach ($this->_client->keys($key) as $i => $_key) {
            $this->_client->delete($_key);
        }

    }

    /**
     * Purges Redis
     * @return bool
     */
    public function flush(): bool
    {
        return $this->_client->flushAll();
    }

}
